# merlin.iot Documentation

![ ](/images/merlin_barra.jpg "")

## About the project

merlin.iot is a comprehensive S2aaS (sensing-as-a-service) solution for monitoring critical variables and automating verification processes for specific application in labs of clinical analysis, healthcare, pharmaceutical and food industry.

## Project status
The project is still in development.

## Authors and acknowledgment
TECSCI https://tecsci.com.ar/
SGO consulting
